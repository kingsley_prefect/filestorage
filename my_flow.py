# flows/my_flow.py

from prefect import task, Flow
from prefect.environments.storage import Bitbucket
# from atlassian import Bitbucket

@task
def get_data():
    return [1, 2, 3, 4, 5]

@task
def print_data(data):
    print(data)

with Flow("file-based-flow") as flow:
    data = get_data()
    print_data(data)


# Bitbucket.get_content_of_file("project_prefect_demo", "kingsley_prefect/project_prefect_demo","myflow/my_flow.py","master")


flow.storage = Bitbucket(
    project="project_prefect_demo",
    repo="kingsley_prefect/project_prefect_demo",                 # name of repo
    path="README.md",        # location of flow file in repo
    secrets=["BITBUCKET_ACCESS_TOKEN"]  # name of personal access token secret
)

# flow.run()

flow.register("bitbucket")
